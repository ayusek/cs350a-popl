% Function Declarations
declare
CompMain
CompRec
Noteq
GetParent
GetValue
BindRecords
ConvertRecord
ConvertList
BindRecMain
BindClass
BindValue
BindMain
Lessthan
FeatureMatch
Inlist
declare
Sas = {Dictionary.new}

declare
Dic = {Dictionary.new}


declare
Rec1
Rec2

declare
MyUndeclaredVariable19950523 %To suspend Oz statements in our implementations
%=============================================
%=====Files Related To SAS ===================
%=============================================

declare
Num={NewCell 0}
Auxlist={NewCell nil}
declare
RecordBoundFlag={NewCell 1} % used for pattern matching

declare
fun {Getent}
   Num:=@Num+1
   [nULL @Num]
end

fun {Inlist Xs Y}
   case Xs
   of nil then false
   [] Y|Xr then true
   else
      {Inlist Xs.2 Y}
   end
end

% =============Record Operations =====================

declare % Compares literals for records
fun {Lessthan X Y}
   X.1.1<Y.1.1
end

fun {ConvertList Xs E}  % Converts a record to be stored in the SAS accordingly
   {List.map Xs fun {$ X} case X.2.1
			  of ident(Y) then [X.1 E.Y]
			  [] record|Xr then [X.1 {ConvertRecord X.2.1 E}]
			  else X end end}
end

fun {ConvertRecord Rec E}
   Rec.1|Rec.2.1|{Sort {ConvertList Rec.2.2.1 E} Lessthan}|nil
end

fun {CompMain Xs Ys E}
   case Xs
   of nil then true
   else
      local X Y in
	 X=Xs.1
	 Y=Ys.1
	 if X.1.1 \= Y.1.1 then false
	 elseif {Noteq X.2.1 Y.2.1 E} then false
	 else
	    {CompMain Xs.2 Ys.2 E}
	 end
      end
   end
end

fun {CompRec R1 R2 E}
   if R1.2.1.1\=R2.2.1.1 then false
   else
      if {Length R1.2.2.1}\={Length R2.2.2.1} then false
      else
	{CompMain R1.2.2.1 R2.2.2.1 E}
      end
   end
end

fun {Noteq Xs Ys E}
   local Xval Yval in
      case Xs
      of literal(X) then
	 case Ys
	 of literal(Y) then X\=Y
	 else
	    Xs\={GetValue Ys}
	 end
      else
	 Xval = {GetValue Xs}
	 case Ys
	 of literal(Y) then Xval\=Ys
	 else
	    Yval={GetValue Ys}
	    if {Or Xval.1\=record Yval.1\=record} then Xval\=Yval
	    else
	      {CompRec Xval Yval E}
	    end
	 end
      end
   end
end

%========= Report Operations End ===========================
%========= SAS Operations ==================================


%Sas.X.2 returns a single element list instead of a value
fun {GetParent X}
   if Sas.X.1 \= nULL then X
   elseif Sas.X.2.1 == X then X
   else
      local Px in
	 Px={GetParent Sas.X.2.1}
	 {Dictionary.put Sas X [nULL Px]}  % Path compression in union find
	 Px
      end
   end
end

fun {GetValue X}
   local Px in
      Px={GetParent X}
      if Sas.Px.1 == nULL then {Browse unboundedVariable} nULL
      else Sas.Px.1 end
      end
end

fun {FeatureMatch Xs Ys} % Just Compares the names of the features for two records
   case Xs
   of nil then true
   else
      {And Xs.1.1 == Ys.1.1 {FeatureMatch Xs.2 Ys.2}}
   end
end

proc {BindRecMain Xs Ys} % sorted and mapped according to Sas.
   case Xs # Ys
   of nil # nil then skip
   [] (X|Xr) # (Y|Yr) then
      if {And {IsInt X.2.1} {IsInt Y.2.1}} then
	 if {Not {Inlist @Auxlist [X.2.1 Y.2.1]}} then {BindClass X.2.1 Y.2.1} Auxlist:=[X.2.1 Y.2.1]|[Y.2.1 X.2.1]|@Auxlist
	    else skip end
      elseif {IsInt X.2.1} then {BindValue X.2.1 Y.2.1} % Y.2.1 being records would be handled in bind value
      elseif {IsInt Y.2.1} then {BindValue Y.2.1 X.2.1} %symmetric
      else
	  if {Or X.2.1.1\=record Y.2.1.1\=record} then
	       if X.2.1 \= Y.2.1 then {Browse variableboundError} if MyUndeclaredVariable19950523 then skip else skip end %You need to wait here
	       else skip end
	  else
	       {BindRecords X.2.1 Y.2.1}
	  end
      end
      {BindRecMain Xr Yr}
   end
end



% For binding two records together
proc {BindRecords R1 R2}
   if R1.2.1.1\=R2.2.1.1 then  {Browse recordLabelsDontMatch} if MyUndeclaredVariable19950523 then skip else skip end %You need to wait here
   elseif {Length R1.2.2.1}\={Length R2.2.2.1}  then {Browse recordLengthDontMatch} if MyUndeclaredVariable19950523 then skip else skip end %You need to wait here
   elseif {FeatureMatch R1.2.2.1 R2.2.2.1} then {BindRecMain R1.2.2.1 R2.2.2.1} %these are lists of literals
   else  {Browse featuresDontMatch} if MyUndeclaredVariable19950523 then skip else skip end %You need to wait here
   end
end

proc {BindValue X V}
   local Px in
      Px = {GetParent X}
      if Sas.Px.1==nULL then {Dictionary.put Sas Px [V] }
      else
	 case Sas.Px.1.1#V.1
	 of (Xa|Xr)#(Y|Yr) then
	     if {And Xa==procedure Y==procedure} then {Browse unificationerror} if MyUndeclaredVariable19950523 then skip else skip end
	    end
	    else skip end   
	 
	 if {Or Sas.Px.1.1\=record V.1\=record} then
	    if Sas.Px.1 \= V then {Browse variableboundError} if MyUndeclaredVariable19950523 then skip else skip end %You need to wait here
	    else  skip end
	 else
	      {BindRecords Sas.Px.1 V}
	 end
      end
   end
end

proc {BindClass X Y}
   local Px Py in
      Px = {GetParent X}
      Py = {GetParent Y}
      if Px == Py then skip
      elseif {And Sas.Px.1==nULL Sas.Py.1==nULL} then
	 {Dictionary.put Sas Px [nULL Py]}
      elseif {And Sas.Px.1 \= nULL Sas.Py.1==nULL} then
	 {Dictionary.put Sas Py [nULL Px]}
      elseif {And Sas.Py.1 \= nULL Sas.Px.1==nULL} then
	 {Dictionary.put Sas Px [nULL Py]}
      else
	  if {Or Sas.Px.1.1\=record Sas.Py.1.1\=record} then
	       if Sas.Px.1 \= Sas.Py.1 then {Browse variableboundError} if MyUndeclaredVariable19950523 then skip else skip end %You need to wait here
	       else skip end
	    else
	     Auxlist:=[Y X]|[X Y]|@Auxlist
	     {BindRecords Sas.Px.1 Sas.Py.1}
	    end
      end
   end
end

%Dic is assumed to be the environment, so obviously this code is supposed to be integrated into the environment or pass the dictionary into the function as I have done in this case

%Decides the types of values Xs and Ys and according ly calls what is to be done with them
%Xs and Ys are variables

proc {BindMain Xso Yso E}
   local Xs Ys in
      if Xso.1 == record then Xs = {ConvertRecord Xso E} 
      elseif Xso.1 == procedure then Xs = [Xso E] 
      else Xs = Xso end
      if Yso.1 == record then Ys = {ConvertRecord Yso E} 
      elseif Yso.1 == procedure then Ys = [Yso E]
      else Ys = Yso end
      case Xs
      of ident(X) then
	 case Ys
	 of ident(Y) then {BindClass E.X E.Y}
	 else
	    {BindValue E.X Ys} % here variables are dictionary pointers
	 end
      else
	 case Ys
	 of ident(Y) then {BindValue E.Y Xs}
	 else
	    if {And Xso.1==procedure Yso.1==procedure} then {Browse unificationerror} if MyUndeclaredVariable19950523 then skip else skip end %You need to wait here
	    end
	    if {Or Xs.1\=record Ys.1\=record} then
	       if Xs \= Ys then {Browse variableReboundError} if MyUndeclaredVariable19950523 then skip else skip end %You need to wait here
	       else skip end
	    else
	       {BindRecords Xs Ys}
	    end
	 end
      end
   end
end




%==============CREATING ENVIORNMENT =====
%============== TO ADD RECORD ELEMENTS TO ENVIORNMENT =======
declare
proc {AddToEnviornment List New}
   case List
   of nil then skip
   [] X|Xr then
      {Dictionary.put New X.2.1.1 @Num + 1}
      {Dictionary.put Sas @Num + 1 {Getent}}
      {AddToEnviornment Xr New}
   end
end

declare
proc {AddToEnviornment2 Xs Ys Envnew Envold}
   case Xs#Ys 
   of nil#nil then skip
   [] (X|Xr)#(Y|Yr) then
      {Dictionary.put Envnew X.1 @Num + 1}
      {Dictionary.put Sas @Num + 1 {Getent}}
      {Dictionary.put Sas Envnew.(X.1) [nULL Envold.(Y.1)]}
      {AddToEnviornment2 Xr Yr Envnew Envold}
   end
end

declare
proc {Evaluate Ss E}
   Auxlist:=nil%setting auxlist to be nil just in case bind records come
   local New in 
   New = {Dictionary.clone E} % creating a new dictionary
   if Ss == nil then skip else {Browse Ss} end %printing the statement correctly running

   % Handling each statements
   case Ss
   of nil then skip
   [] nop|Sr then {Evaluate Sr New}
   [] localvar|Sr then
      {Dictionary.put New Sr.1.1 @Num + 1}
      {Dictionary.put Sas @Num + 1 {Getent}}
      {Evaluate Sr.2 New}
   [] bind|Sr then  {BindMain Sr.1 Sr.2.1 New}   %handling variable binding
   [] conditional|ident(X)|Sr then local Val in Val = {GetValue E.X} 
      if Val == nULL then {Browse varibaleboundError} if MyUndeclaredVariable19950523 then skip else skip end %You need to wait here
      elseif Val == literal(t) then {Evaluate Sr.1 New}
      else {Evaluate Sr.2.1 New} end
				   end
   [] match|ident(X)|Rec|Sr then
      local R1 R2 in
	 R1 = {GetValue E.X}
	 R2 = Rec
	 RecordBoundFlag := 1
	 if R1.1 == record then
	 if R1.2.1.1\=R2.2.1.1 then RecordBoundFlag := 0 
	 elseif {Length R1.2.2.1}\={Length R2.2.2.1} then RecordBoundFlag := 0 
	 elseif {Not {FeatureMatch R1.2.2.1 R2.2.2.1}} then RecordBoundFlag := 0 
	 end
	 else
	    RecordBoundFlag := 0
	 end
	 if @RecordBoundFlag == 1 then
	    {AddToEnviornment R2.2.2.1 New}
	    {BindMain R1 R2 New} %these are lists of literals, They are bound together
	    {Evaluate Sr.1 New}
	 else
	    {Evaluate Sr.2.1 New}
	    RecordBoundFlag :=1
	 end
      end
   [] apply|ident(X)|Sr then
      local Val Env in
	 Val = {GetValue New.X}
	 case Val.1 of procedure|Xr then
	    if {Length Sr}\={Length Xr.1} then {Browse illegalarity} end
	    Env=Val.2.1
	    local New2 in
	       New2 = {Dictionary.clone Env}
	       {AddToEnviornment2 Xr.1 Sr New2 New}
	       {Evaluate Xr.2.1 New2}
	    end
	 else
	    {Browse notaprocedure} if MyUndeclaredVariable19950523 then skip else skip end %You need to wait here
	 end
      end
      
   else % When not any of the above forms
      {Evaluate Ss.1 New}
      local New2 in  %passing seprate enviornments to the two functions.
	 New2 = {Dictionary.clone New} 
	 {Evaluate Ss.2 New2}
      end
   end
   end
end



{Browse 1}
{Browse {Dictionary.entries Sas}}
