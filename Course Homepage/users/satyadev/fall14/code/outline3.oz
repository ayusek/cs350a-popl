%===================================
% List Functions: Reverse a list
%===================================
declare
fun {Reverse Xs}
   case Xs
   of nil then nil
   [] X|Xr then {List.append {Reverse Xr} [X]}
   end
end

{Browse {Reverse [a b c d]}}

declare
fun {ReverseIter Xs PartialList}
   case Xs
   of nil then PartialList
   [] X|Xr then {ReverseIter Xr X|PartialList}
   end
end

{Browse {ReverseIter [a b c d] nil}}

declare
fun {Reverse3 Xs}
   case Xs
   of nil then nil
   [] X|Xr then {Reverse3 Xr}|X
   end
end


{Browse {Reverse3 [a b c]}}




      

      
%====================================
% Map function
% F    : Unary function
%====================================
declare
fun {Map F Xs}
   case Xs
   of nil then nil
   [] X|Xr then {F X}|{Map F Xr}
   end
end

% passing an anonymous function

{Browse {Map fun {$ X}
		local Y in
		   Y = X*X
		   Y
		end
	     end
	 [1 2 3]}}



%====================================
% FoldL: fold for left-associative
%        binary functions
% F    : Binary function
%====================================
declare
fun {FoldL1 Partial F Xs}
   case Xs
   of nil then Partial
   [] X|Xr then {FoldL1 {F Partial X} F Xr}
   end
end

{Browse {FoldL1 0 fun {$ X Y} X + Y end {Map
					 fun {$ X} 1 end
					 [1 2 3]}}}





   
%=======================================
% Try out the following functions
%   1. sum of squares of elements of a list
%   2. Length of a list
%   3. Append
%=======================================

%===============================
% Partially Evaluated Functions
% Returning functions
% Currying
% Anonymous functions
%==============================

declare
fun {IncBy X}
   fun {$ Y}
      X+Y
   end
end

{Browse {IncBy 2}}
      





      

%==============================
% Recursive Data Structures
%==============================

%===============================
% A binary search tree is defined as
%          nil (OR)
%          bst(node:X left :bst(..)
%                     right:bst(..))
%===============================

% Example
declare
T = bst(node:2
	left:bst(node:1
		 left:nil
		 right:nil)
	right:bst(node:4
		  left:bst(node:3
			   left:nil
			   right:nil)
		  right:bst(node:5
			    left:nil
			    right:nil)))

%===========================
% A General rule: follow the definition!
%===========================
declare
proc {InOrder T}
   case T
   of nil then skip
   [] bst(node:X left:L right:R) then
      {InOrder L}
      {Browse X}
      {InOrder R}
   end
end

{InOrder T}
   


% Merge two bsts?

%==========================
% Self-referential data
%==========================
declare Inf
Inf = 1|Inf
{Browse Inf}

%===================================================
% Lazy Evaluation
%===================================================
declare
fun lazy {IntsFrom N}
   N|{IntsFrom N+1}
end


{Browse {IntsFrom 1}.2.2.1}
/*
code4.oz
*/